﻿using UnityEngine;
using System.Collections;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Scene2_Global : MonoBehaviour
{
    private string filename = @"C:\Projects\Unity CSV Test 1\Assets\CSV\vr project csv scene2.csv";

    public List<double[]> data;

    public Slider slider;

    string[] column_descriptions;
    public Text Data_Raw;

    public ParticleSystem[] obj_array;

    // Use this for initialization
    void Start()
    {
        print("Setting Data");
        data = new List<double[]>();
        Read_CSV();
        slider.maxValue = data.Count - 1;
        print("Max: " + slider.maxValue.ToString());
        set_Data_Raw(0);

        set_obj_color(obj_array[0], (int)data[0][3]);
        set_obj_color(obj_array[1], (int)data[0][4]);
        set_obj_color(obj_array[2], (int)data[0][5]);
        set_obj_color(obj_array[3], (int)data[0][6]);
        set_obj_color(obj_array[4], (int)data[0][7]);
        set_obj_color(obj_array[5], (int)data[0][7]);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform.name == "Cube (0)")
                {
                    SceneManager.LoadScene("Scene0");
                }
                else if (hit.transform.name == "Cube (1)")
                {
                    SceneManager.LoadScene("Scene1");
                }
            }
        }
    }

    public void Back()
    {
        SceneManager.LoadScene("main");
    }

    private void set_obj_color(ParticleSystem obj, int set)
    {
        if (set >= 1)
        {
            obj.startColor = Color.green;
        }
        else
        {
            obj.startColor = Color.red;
        }
    }

    private void set_Data_Raw(int index_in)
    {
        string str_temp = "";
        for (int i = 0; i < column_descriptions.Length; i++)
        {
            str_temp += column_descriptions[i] + ": " + data[index_in][i].ToString() + "\n";
        }
        Data_Raw.text = str_temp;
    }

    public void SliderChange(float newIndex)
    {
        int index = (int)(newIndex);
        set_Data_Raw(index);

        set_obj_color(obj_array[0], (int)data[index][3]);
        set_obj_color(obj_array[1], (int)data[index][4]);
        set_obj_color(obj_array[2], (int)data[index][5]);
        set_obj_color(obj_array[3], (int)data[index][6]);
        set_obj_color(obj_array[4], (int)data[index][7]);
        set_obj_color(obj_array[5], (int)data[index][7]);
    }

    public void Read_CSV()
    {
        StreamReader reader = new StreamReader(filename);
        string[] lines = reader.ReadToEnd().Split('\n');

        column_descriptions = lines[0].Split(',');

        for (int line_index = 1; line_index < lines.Length; line_index++)
        {
            string[] cols = lines[line_index].Split(',');
            if (cols.Length > 1)
            {
                double[] values = new double[cols.Length];
                for (int col_index = 0; col_index < cols.Length; col_index++)
                {
                    cols[col_index] = cols[col_index].Replace("\r", "");
                    Double.TryParse(cols[col_index], out values[col_index]);
                }
                data.Add(values);
            }
        }
    }

}

