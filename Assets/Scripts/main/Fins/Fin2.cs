﻿using UnityEngine;
using System.Collections;

public class Fin2 : MonoBehaviour
{
    float rotation = 0.0f;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        float f1 = (2.0f * Random.value) - 1.0f;
        if ((rotation + f1) < -45f)
        {
            f1 = 0;
        }
        else if ((rotation + f1) > 45f)
        {
            f1 = 0;
        }
        rotation += f1;
        transform.Rotate(0, 0, f1);
    }
}
