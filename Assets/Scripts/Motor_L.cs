﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Motor_L : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            bool success = Physics.Raycast(ray, out hit);
            if (success)
            {
                if (hit.transform.name == "Motor_L")
                {
                    SceneManager.LoadScene("Scene2");
                }
            }
        }
    }

    Color Color0;

    void OnMouseEnter()
    {
        Color0 = GetComponent<Renderer>().material.color;
        GetComponent<Renderer>().material.color = Color.red;
    }

    void OnMouseExit()
    {
        GetComponent<Renderer>().material.color = Color0;
    }
}
